# KJ ( Konnect Jerlah )
KJ is a bash script to manage SSH connection. KJ stands for "Konnect Jerlah". It is a Malay slang which means "Just connect to it!!". KJ is distributed under GNU GPL License. 

## 1. Get KJ
```sh
git clone https://gitlab.com/aburayyan/kj.git
```

## 2. Install KJ
```ssh
sudo mkdir /usr/local/bin
```
* It is OK if there is warning that the /usr/local/bin already exist

```ssh
sudo cp kj/src/kj /usr/local/bin/kj
```

## 3. Config KJ
```sh
vi ~/.kj.cfg
```

.kj.cfg format is as follows
'userid' 'server ip' 'Remarks-without-space'

example
```sh
jeffry 172.2.4.13 PlatformJH
ubuntu 172.2.4.13 FinanceUbuntuJH
ec2-user 202.111.3.4 MyAwsFreeAcc
```

## 4. Use KJ
Just issue the kj
```sh
kj
```

Then choose the connection that you want base on its number

![img](img/Screenshot_2022-03-15_at_11.22.20_PM.png)

## 5. Password less connection
If you want a password less connection you need to use the SSH keys method. 

